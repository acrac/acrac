---
title: "Vídeos"
date: 2020-04-11
thumbnail: "img/biglogo.png"
tags:
  - "youtube"

categories:
  - "Vídeos"

menu: main
draft: false
---

<p style="font-size:150%">Seleção de vídeos com alguns dos melhores momentos da nossa equipa.</p>

<!--more-->

## ACRAC 2015

{{< youtube k2cXiZzPtA0 >}}

<br></br>
## Liga dos últimos I

{{< youtube 97SXUN1-l6o >}}

<br></br>
## Liga dos últimos II

{{< youtube Uc9dBt0MUnw >}}

<br></br>
## ACRAC vs ACCacém - 30 Ago 2015

{{< youtube Z2N3i1aQl2I >}}

<br></br>
## Juvenis - ACRAC x SCC - 20 Nov 2016

{{< youtube kdsydZ1EXY4 >}}

<br></br>
## Atalaia do Campo vs Sporting Covilhã B - 23 Fev 2020

{{< youtube h8Abiyzl9V8 >}}
